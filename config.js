module.exports = {
  endpoint: 'https://self-hosted.gitlab/api/v4/',
  token: RENOVATE_TOKEN,
  platform: 'gitlab',
  onboardingConfig: {
    extends: ['config:base'],
  },
  repositories: ['zeelandnet/Kassa'],
};
